import dptools as dp

data_1 = [ [1,2,3], [2,4] ]
data_2 = [ [1,2], [5] ]
data_3 = [ [1,2,5], [5] ]
data_4 = [ [2,4,5], [1,4] ]
dataset = [data_1, data_2, data_3, data_4]

print "--- Dataset 1 ---"
for data in dataset:
    print data

min_support = 3
frequent_sets = dp.max_persistent(min_support, dataset)
print "frequent sets:\t" + str(frequent_sets)

class_1 = [0, 3]
class_2 = [1, 2]
most_discrim = dp.most_discriminative(class_1, class_2, dataset)
print "most discriminative:\t" + str(most_discrim)



data_1 = [ ['a','a','b'] ]
data_2 = [ ['a','b','c'] ]
dataset = [data_1, data_2]

print ""
print "--- Dataset 2 ---"
for data in dataset:
    print data

min_support = 1
frequent_sets_dup = dp.max_persistent(min_support, dataset, duplicates=True)
print "frequent sets (dups):\t" + str(frequent_sets_dup)

class_1 = [0]
class_2 = [1]
most_discrim = dp.most_discriminative(class_1, class_2, dataset)
print "most discriminative:\t" + str(most_discrim)
