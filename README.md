Discriminative Itemset Mining
=============================
A pure python implementation of an algorithm for mining *discriminative* itemsets.

Usage
-----
To use as a python library:
```
>>> import dptools as dp
```

An example using the module in a python script can be found in `example.py`.
