import collections

def build_tree(communities, minsup=0):
    """ 
    Creates an DPTree for the given itemsets and minimum support.
    
    Parameters
    ----------
    communities: triple nested list of strings
        list of communities (lists of strings) for each graph
    minsup : int
        The minimum support threshold.

    Returns 
    -------
    tree : DPTree
    rank : dictionary
        Maps each node (string) to an int to define an ordering.
    """
    ngraphs = len(communities)

    # Find total number of appearances of every node
    count = collections.defaultdict(int)
    gcount = collections.defaultdict(int)
    for i in range(ngraphs):
        done = set([])
        for community in communities[i]:
            for v in community:
                if not v in done:
                    done.add(v)
                    gcount[v] += 1
                count[v] += 1
    
    # Rank nodes so that low appearance comes first
    nodes = sorted([v for v in count if gcount[v] >= minsup], key=count.get)
    rank = {v:i for i,v in enumerate(nodes)}    # break up any ties
    
    # Insert all communities into DPTree
    tree = DPTree(rank)
    tree.cond_labels = set(range(ngraphs))
    for i in range(ngraphs):
        for community in communities[i]:
            comm = sorted([v for v in community if v in rank], key=rank.get, reverse=True)
            tree.insert_itemset(comm, {i})
    
    return tree



class DPTree(object):
    def __init__(self, rank=None):
        self.root = DPNode(None, set([]))
        self.nodes = collections.defaultdict(list)
        self.cond_items = []
        self.cond_labels = set([])
        self.rank = rank


    def labels(self, item):
        labels = set([])
        for n in self.nodes[item]:
            labels |= n.labels
        return labels
    
    
    def support(self, item):
        return len(self.labels(item))
    
    
    def edge_labels(self, item, elabels):
        labels = set([])
        for v in self.cond_items:
            edge = (v, item)
            if item < v:
                edge = (item, v)
            labels |= elabels[edge]
        return labels 
    
    
    def conditional_tree(self, cond_item, minsup=0, pclass=None):
        """ 
        Creates and returns the subtree of self conditioned on cond_item.
        
        Parameters
        ----------
        cond_item : int | str
            Item that the tree (self) will be conditioned on.
        minsup : int 
            Minimum support threshold.

        Returns
        -------
        cond_tree : DPTree
        """
        # Find all path from root node to nodes for item
        branches = []
        labels = collections.defaultdict(set)
        count = collections.defaultdict(int)
        for node in self.nodes[cond_item]:
            branch = node.itempath_to_root(reverse=True)
            branches.append(branch)
            for item in branch:
                labels[item] |= node.labels
                count[item] += node.count
        
        # Define new ordering
        if pclass == None:
            lcount = {item:len(labels[item]) for item in labels}
        else:
            lcount = {item:len(labels[item]&pclass) for item in labels}
        items = [item for item in lcount if lcount[item] >= minsup]
        items.sort(key=lcount.get)
        rank = {item:i for i,item in enumerate(items)}

        # Create conditional tree
        cond_tree = DPTree(rank)
        for i,branch in enumerate(branches):
            branch = sorted([item for item in branch if item in rank], key=rank.get, reverse=True)
            cond_labels = self.nodes[cond_item][i].labels
            cond_count = self.nodes[cond_item][i].count
            cond_tree.insert_itemset(branch, cond_labels, cond_count)
        cond_tree.cond_items = self.cond_items + [cond_item]
        
        return cond_tree
    
    
    def insert_itemset(self, itemset, labels, count=1):
        """ 
        Inserts a list of items into the tree.
        
        Parameters
        ----------
        itemset : list 
            Items that will be inserted into the tree.
        count : int
            The number of occurrences of the itemset.
        """
        # Root will keep track of all labels in the tree
        self.root.labels |= labels
        
        # Follow existing path in tree as long as possible
        index = 0
        node = self.root
        for item in itemset:
            if item in node.children:
                child = node.children[item]
                child.labels |= labels
                child.count += count
                node = child
                index += 1
            else:
                break
        
        # Insert any remaining items
        for item in itemset[index:]:
            child_node = DPNode(item, labels.copy(), count, node)
            self.nodes[item].append(child_node)
            node = child_node
        

    @property
    def is_path(self):
        if len(self.root.children) > 1:
            return False
        for i in self.nodes:
            if len(self.nodes[i])>1 or len(self.nodes[i][0].children)>1:
                return False
        return True
    
    
    @property
    def items(self):
        items = []
        for i in self.nodes:
            items += [i]*len(self.nodes[i])
        return items


    def write_dot(self):
        """ Writes the tree to a dot file for use in GraphViz. """
        with open("graph.dot", "w") as dotfile:
            dotfile.write("digraph G {\n\n")
            dotfile.write("margin=\"0,0\";\n")
            dotfile.write("node [fillcolor=gray]\n\n")
     
            nodes = []
            for item in self.nodes.keys():
                nodes += self.nodes[item]
            nodes.append(self.root)
            
            node_map = {}
            for i,node in enumerate(nodes):
                node_map[node] = i
                dotfile.write(str(i)+" [label=\""+str(node.item)+": {")
                for j,label in enumerate(node.labels):
                    dotfile.write(str(label))
                    if j < len(node.labels)-1:
                        dotfile.write(",")
                dotfile.write("}\", shape=circle]\n")
            dotfile.write("\n\n")
    
            for i,node in enumerate(nodes):
                parent = node.parent
                if not parent == None:
                    dotfile.write(str(node_map[parent])+"->"+str(i)+"\n")
    
            dotfile.write("\n\n}")






class DPNode(object):
    def __init__(self, item, labels, count=1, parent=None):
        self.item = item    
        self.count = count
        self.labels = labels
        self.parent = parent
        self.children = collections.defaultdict(DPNode)
        
        if parent != None:
            parent.children[item] = self


    def itempath_to_root(self, reverse=False):
        """ Returns the top-down sequence of items from self to (but not including) the root node. """
        path = []
        node = self.parent
        while node.item != None:
            path.append(node.item)
            node = node.parent
        
        if reverse:
            path.reverse()
        
        return path

    
    def all_children(self):
        nodes = self.children.values()
        for child in nodes:
            nodes += child.all_children()
        return nodes




class MFITree(object):
    def __init__(self, rank):
        self.root = self.Node(None)
        self.nodes = collections.defaultdict(list)
        self.cache = None
        self.rank = rank
    
    
    def insert_itemset(self, itemset):
        """ 
        Inserts a list of items into the tree.
        
        Parameters
        ----------
        itemset : list 
            Items that will be inserted into the tree.
        """
        # Follow existing path in tree as long as possible
        index = 0
        node = self.root
        for item in itemset:
            if item in node.children:
                child = node.children[item]
                node = child
                index += 1
            else:
                break
        
        # Insert any remaining items
        for item in itemset[index:]:
            child_node = self.Node(item, node)
            self.nodes[item].append(child_node)
            node = child_node

    
    def contains(self, itemset):
        """
        Checks if this tree contains itemset in one of its branches. 
        The algorithm assumes that itemset is sorted according to self.rank.
        """
        if itemset==[] or itemset==None:
            return True

        if self.cache != None:
            i = 0
            for item in reversed(self.cache):
                if self.rank[itemset[i]] < self.rank[item]:
                    break
                if itemset[i] == item:
                    i += 1
                if i == len(itemset):
                    return True
                
        for basenode in self.nodes[itemset[0]]:
            i=0
            node = basenode
            while node.item != None:
                if self.rank[itemset[i]] < self.rank[node.item]:
                    break
                if itemset[i] == node.item:
                    i += 1
                if i == len(itemset):
                    return True
                node = node.parent
        return False
    
    
    class Node(object):
        def __init__(self, item, parent=None):
            self.item = item    
            self.parent = parent
            self.children = collections.defaultdict(MFITree.Node)
            
            if parent != None:
                parent.children[item] = self
