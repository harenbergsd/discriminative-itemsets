from .discriminative import *
from .persistent import *


__author__ = "Steve Harenberg <harenbergsd@gmail.com>"


def write_itemsets(itemsets, filename):
    """ Writes set of communities to a file. Each line will be a community with nodes separated by spaces. """
    with open(filename, 'w') as f:
        for itemset in itemsets:
            string = ""
            for i in range(len(itemset)):
                string += str(itemset[i])
                if not i == len(itemset)-1:
                    string += ","
            string += "\n"
            f.write(string)
