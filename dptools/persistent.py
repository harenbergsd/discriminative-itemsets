from .tree import *


def max_persistent(minsup, communities=None, tree=None, duplicates=False):
    if communities==None and tree==None:
        return
    minsup = int(minsup)
    if communities != None:
        tree = build_tree(communities, minsup)
    
    items = tree.nodes.keys()
    mfit = MFITree(tree.rank)
    
    return [c for c in psets(tree, minsup, mfit, duplicates)]


def discrim_enum(minsup_pos, minsup_neg, pclass, nclass, communities=None, tree=None):
    if communities==None and tree==None:
        return
    minsup_pos = int(minsup_pos)
    minsup_neg = int(minsup_neg)
    pclass = set(pclass)
    nclass = set(nclass)
    if communities != None:
        tree = build_tree(communities, minsup_pos)
    
    items = tree.nodes.keys()
    mfit = MFITree(tree.rank)
    
    return [c for c in dsets(tree, mfit, minsup_pos, minsup_neg, pclass, nclass)]


def dsets(tree, mfit, minsup_pos, minsup_neg, pclass, nclass):
    items = list(tree.nodes)
    largest_set = sorted(tree.cond_items+items, key=mfit.rank.get)

    if tree.is_path:
        if not mfit.contains(largest_set):
            largest_set.reverse()
            mfit.cache = largest_set
            mfit.insert_itemset(largest_set)
            labels = tree.root.labels
            if len(tree.nodes) > 0:
                labels = tree.nodes[min(items, key=tree.rank.get)][0].labels
            if len(labels & nclass) <= minsup_neg:
                yield largest_set
    else:
        # Loop over each item in tree creating another conditional tree
        items.sort(key=tree.rank.get)
        for item in items:
            # Check if the tree will produce a subset already produced
            if mfit.contains(largest_set):
                return 
            
            # Update largest set since it can no longer contain item
            largest_set.remove(item)

            # Generate conditional tree and recurse
            cond_tree = tree.conditional_tree(item, minsup_pos, pclass)
            for mfi in dsets(cond_tree, mfit, minsup_pos, minsup_neg, pclass, nclass):
                yield mfi


def psets(tree, minsup, mfit, duplicates=False):
    """
    Parameters
    ----------
    tree : DPTree
    minsup : int
    mfit : MFITree
        Keeps track of what itemsets have already been output.

    Yields
    ------
    lists of strings
        *Maximal* Set of nodes that has occurred in minsup communities (across graphs).
    """
    items = list(tree.nodes)
    if not duplicates:
        largest_set = sorted(tree.cond_items+items, key=mfit.rank.get)
    else:
        largest_set = sorted(tree.cond_items+tree.items, key=mfit.rank.get)

    if tree.is_path:
        if not mfit.contains(largest_set):
            largest_set.reverse()
            mfit.cache = largest_set
            mfit.insert_itemset(largest_set)
            yield largest_set
    else:
        # Loop over each item in tree creating another conditional tree
        items.sort(key=tree.rank.get)
        for item in items:
            # Check if the tree will produce a subset already produced
            if mfit.contains(largest_set):
                return 
            
            # Update largest set since it can no longer contain item
            if not duplicates:
                largest_set.remove(item)
            else:
                largest_set = [i for i in largest_set if i!=item]

            # Generate conditional tree and recurse
            cond_tree = tree.conditional_tree(item, minsup)
            for mfi in psets(cond_tree, minsup, mfit, duplicates):
                yield mfi
