import math
from .tree import *

# Global variables used in the algorithm
DSET    = None
SCORE   = None
LABELS  = None


def most_discriminative(pclass, nclass, communities=None, tree=None, minsup=0):
    global DSET, SCORE, LABELS
    DSET = set([])
    LABELS = set([])

    # Make sure input arguments are valid and convert them as needed
    if communities==None and tree==None:
        return
    if communities != None:
        tree = build_tree(communities, minsup)

    if type(pclass) == list:
        pclass = set(pclass)
    if type(nclass) == list:
        nclass = set(nclass)

    # Determine next most discriminative pattern
    lbound = probe(tree, pclass, nclass, minsup) - 0.0000001
    SCORE = lbound
    dset(tree, pclass, nclass, minsup)
    
    return DSET, SCORE, LABELS



def dset(tree, pclass, nclass, minsup=0):
    global DSET, SCORE, LABELS
    
    items = sorted(tree.nodes.keys(), key=tree.rank.get, reverse=True)
    for item in items:
        labels = tree.labels(item)
        psup = len(labels & pclass)
        nsup = len(labels & nclass)
        
        if psup+nsup < minsup:
            continue

        score = information_gain(len(pclass), len(nclass), psup, nsup)
        
        if score > SCORE:
            SCORE = score
            DSET = tree.cond_items + [item]
            LABELS = labels
        
        upperbound = ig_upperbound(len(pclass), len(nclass), psup, nsup)
        if upperbound > SCORE:  # Adding this item can possibly improve the discrimination score
            cond_tree = tree.conditional_tree(item, minsup)
            dset(cond_tree, pclass, nclass, minsup)


        
def probe(tree, pclass, nclass, minsup=0, best_score=-1):
    """
    Fast greedy algorithm
    """
    items = tree.nodes.keys()
    
    score = {}
    for item in items:
        labels = tree.labels(item)
        psup = len(labels & pclass)
        nsup = len(labels & nclass)
        
        if psup+nsup < minsup:
            continue

        score[item] = information_gain(len(pclass), len(nclass), psup, nsup)
    
    if len(score) == 0: # None of the items were valid (e.g., due to minsup)
        return best_score
    
    best_item = max(score, key=score.get)
    if score[best_item] < best_score:   # Adding one item didn't improve the score, so end (because greedy) 
        return best_score
    else:
        cond_tree = tree.conditional_tree(best_item)
        return probe(cond_tree, pclass, nclass, minsup, score[best_item])



def log2(n):
    if n == 0:
        return 0
    return math.log(n,2)



def entropy(m, n):
    tot = float(m + n)
    if tot == 0:
        return 0
    return -1*m/tot*log2(m/tot) - n/tot*log2(n/tot)



def information_gain(gp, gm, gpg, gmg):
    g = float(gp + gm)
    return entropy(gp, gm) - (gpg+gmg)/g*entropy(gpg, gmg) - (g-gpg-gmg)/g*entropy(gp-gpg, gm-gmg)



def ig_upperbound(gp, gm, gpg, gmg):
    g = float(gp + gm)
    ig1 = entropy(gp, gm) - (g-gpg)/g * entropy(gp-gpg, gm)
    ig2 = entropy(gp, gm) - (g-gmg)/g * entropy(gp, gm-gmg)
    return max(ig1, ig2)
